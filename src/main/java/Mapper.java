import org.neo4j.graphdb.Label;

import java.util.HashMap;

/**
 * Created by Martin on 12.11.15.
 */
public class Mapper {
    public static NodeLabels getLabel(String labelString) {
        HashMap<String, NodeLabels> mapper = new HashMap<>();
        mapper.put("Thing", NodeLabels.Thing);
        mapper.put("_META_", NodeLabels._META_);
        mapper.put("Resource", NodeLabels.Resource);
        mapper.put("Institution", NodeLabels.Institution);
        mapper.put("'research_area'Field", NodeLabels.Field);
        mapper.put("Field", NodeLabels.Field);
        mapper.put("Researcher", NodeLabels.Researcher);
        mapper.put("Staff", NodeLabels.Staff);
        mapper.put("Professor", NodeLabels.Professor);
        mapper.put("tata", NodeLabels.tata);

        if (mapper.containsKey(labelString)) {
            return mapper.get(labelString);
        }
        return null;
    }

    public static RelationshipTypes getRelationshipType(String relType) {
        HashMap<String, RelationshipTypes> mapper = new HashMap<>();
        mapper.put("isA", RelationshipTypes.isA);
        mapper.put("isFromInstitution", RelationshipTypes.isFromInstitution);
        mapper.put("expertiseUsedBy", RelationshipTypes.expertiseUsedBy);
        mapper.put("hasContributor", RelationshipTypes.hasContributor);
        mapper.put("hasExpert", RelationshipTypes.hasExpert);
        mapper.put("isDimensionOf", RelationshipTypes.isDimensionOf);
        mapper.put("usesExpertiseFrom", RelationshipTypes.usesExpertiseFrom);
        mapper.put("isPartOf", RelationshipTypes.isPartOf);
        mapper.put("hasPart", RelationshipTypes.hasPart);
        mapper.put("hasApplication", RelationshipTypes.hasApplication);
        mapper.put("hasDimension", RelationshipTypes.hasDimension);
        mapper.put("isAnImplementationOf", RelationshipTypes.isAnImplementationOf);
        mapper.put("isAnApplicationOf", RelationshipTypes.isAnApplicationOf);
        mapper.put("isExpertIn", RelationshipTypes.isExpertIn);
        mapper.put("canContributeTo", RelationshipTypes.canContributeTo);

        if (mapper.containsKey(relType)) {
            return mapper.get(relType);
        }
        return null;
    }
}
