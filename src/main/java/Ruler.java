import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin on 24.11.15.
 */
public class Ruler {
    private final List<String> notAllowedRules;

    public Ruler() {
        this.notAllowedRules = new ArrayList<>();
        notAllowedRules.add("('research_area'Field)-[expertiseUsedBy]->('research_area'Field)");
        notAllowedRules.add("(Resource)-[isResourceOf]->('research_area'Field)");
        notAllowedRules.add("('research_area'Field)-[isPartOf]->('research_area'Field)");
        notAllowedRules.add("('research_area'Field)-[isDimensionOf]->('research_area'Field)");
        notAllowedRules.add("('research_area'Field)-[isAnApplicationOf]->('research_area'Field)");
        notAllowedRules.add("('research_area'Field)-[hasExpert]->(Researcher)");
        notAllowedRules.add("('research_area'Field)-[hasContributor]->(Researcher)");
    }

    public boolean isRuleAllowed(List<String> signatures) {
        for (String signature : signatures) {
            if (notAllowedRules.contains(signature)) {
                return false;
            }
        }
        return true;
    }
}
