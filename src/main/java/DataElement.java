import java.util.List;

/**
 * Created by Martin on 01.12.15.
 */
public abstract class DataElement {
    protected List<String> labels;

    public List<String> getLabels() {
        return labels;
    }

    public String labelsToString() {
       return labelsToString(labels);
    }

    public String labelsToString(List<String> labels) {
        if (! labels.isEmpty()) {
            String labelsString = "{";
            for (String label : labels) {
                labelsString += label + ", ";
            }
            labelsString = labelsString.substring(0, labelsString.length() - 2);
            labelsString += "}";
            return labelsString;
        }
        return "{}";
    }

    protected abstract void setLabels(List<String> labels, String primaryLabels);
    protected abstract void setLabels(String labels, String primaryLabels);
}
