import org.neo4j.graphdb.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin on 01.12.15.
 */
public class DataClass extends DataElement {
    private String primaryLabel;
    private DataClass superClass;

    public DataClass(List<String> labels, String primaryLabel) {
        setLabels(labels, primaryLabel);
    }

    public DataClass(String label, String primaryLabel) {
        setLabels(label, primaryLabel);
    }

    protected void setLabels(String label, String primaryLabel) {
        List<String> labels = new ArrayList<>();
        labels.add(label);
        setLabels(labels, primaryLabel);
    }

    protected void setLabels(List<String> labels, String primaryLabel) {
        this.primaryLabel = primaryLabel;
        this.labels = new ArrayList<>();
        this.labels.add("_META_");
        for (String label : labels) {
            this.labels.add(label);
        }
    }

    public String getPrimaryLabel() {
        return primaryLabel;
    }

    public void setSuperClass(DataClass superClass) {
        this.superClass = superClass;
    }

    public List<String> getLabels() {
        return labels;
    }

    public DataClass getSuperClass() {
        return superClass;
    }

    // Return list of all super classes recursively of the class.
    public List<String> getInheritedClasses() {
        return getInheritedClasses(this);
    }

    // Recursive method to traverse inheritance tree up to the root in order to get all superclasses
    private List<String> getInheritedClasses(DataClass node) {

        List<String> superclasses = new ArrayList<>();
        superclasses.add(node.getPrimaryLabel());

        if (node.getSuperClass() == null) {
            return superclasses;
        }
        superclasses.addAll(getInheritedClasses(node.getSuperClass()));
        return superclasses;
    }

}
