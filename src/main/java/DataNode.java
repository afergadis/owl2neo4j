import org.neo4j.graphdb.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Martin on 12.11.15.
 */
public class DataNode extends DataElement {
    private List<String> primaryLabels;
    private String name;
    private HashMap<String, String> properties;

    public DataNode(String name, List<String> labels, String primaryLabel) {
        this.name = name;
        setLabels(labels, primaryLabel);
        this.properties = new HashMap<>();
    }

    public DataNode(String name) {
        this.name = name;
        labels = new ArrayList<>();
        primaryLabels = new ArrayList<>();
        properties = new HashMap<>();
    }

    protected void setLabels(String label, String primaryLabel) {
        List<String> labels = new ArrayList<>();
        labels.add(label);
        setLabels(labels, primaryLabel);
    }

    protected void setLabels(List<String> labels, String primaryLabel) {
        this.labels = new ArrayList<>();
        for (String label : labels) {
            this.labels.add(label);
        }
        this.primaryLabels = new ArrayList<>();
        this.primaryLabels.add(primaryLabel);
    }

    public void addPrimaryLabel(String primaryLabel) {
        this.primaryLabels.add(primaryLabel);
    }

    public List<String> getPrimaryLabels() {
        return primaryLabels;
    }

    public String getPrimaryLabel() {
        return primaryLabels.get(0);
    }

    public String getName() {
        return name;
    }

    public void addLabel(String label) {
        this.labels.add(label);
    }

    public void addProperty(String propertyName, String propertyValue) {
        properties.put(propertyName, propertyValue);
    }

    public HashMap<String, String> getProperties() {
        return this.properties;
    }

    public String propertiesToString() {
        if (! properties.isEmpty()) {
            String propertiesString = "{";
            for (Map.Entry<String, String> entry : properties.entrySet()) {
                propertiesString += entry.getKey() + ": " + entry.getValue() + ", ";
            }
            propertiesString = propertiesString.substring(0, propertiesString.length() - 2);
            propertiesString += "}";
            return propertiesString;
        }
        return "{}";
    }

    public String primaryLabelsToString() {
        return labelsToString(primaryLabels);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataNode dataNode = (DataNode) o;

        if (name != null ? !name.equals(dataNode.name) : dataNode.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (properties != null ? properties.hashCode() : 0);
        return result;
    }
}
